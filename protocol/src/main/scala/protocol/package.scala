package object protocol {

  sealed trait Domain

  sealed trait Action extends Domain
  object Action {
    case class Balance(userInfoId: Long, currency: String, accountType: String) extends Action
    case class Balances(userInfoId: Long) extends Action
    case class Account(userInfoId: Long, accountType: String, currency: String) extends Action
    case class TransactOperation(from: Account, to: Account, amount: BigDecimal) extends Action
    case class Transact(operations: Seq[TransactOperation]) extends Action
  }

  sealed trait Response extends Domain
  object Response {
    case class TransactResult(error: Boolean, errorCode: Option[String], errorText: Option[String]) extends Response
    case class BalanceResult(currency: String, accountType: String, balance: BigDecimal) extends Response
    case class BalanceResults(balances: Seq[BalanceResult]) extends Response
  }
}
