package protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol
//import test.task.AccountsActor.ActionPerformed
import protocol.Action._
import protocol.Response.{ BalanceResult, BalanceResults, TransactResult }

trait JsonSupport extends SprayJsonSupport {
  // import the default encoders for primitive types (Int, String, Lists etc)
  import DefaultJsonProtocol._

  implicit val accountJsonFormat = jsonFormat3(Account)
  implicit val transactOperationJsonFormat = jsonFormat3(TransactOperation)
  implicit val transactJsonFormat = jsonFormat1(Transact)

  implicit val transactResultJsonFormat = jsonFormat3(TransactResult)
  implicit val balanceResultJsonFormat = jsonFormat3(BalanceResult)
  implicit val balanceResultsJsonFormat = jsonFormat1(BalanceResults)

  //  implicit val actionPerformedJsonFormat = jsonFormat1(ActionPerformed)
}
