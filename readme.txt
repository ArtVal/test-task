1. Preface

Есть структура БД:

CREATE TABLE public.account (
  accountid int8 NOT NULL,
  account varchar(20) NOT NULL,
  "name" varchar(128) NOT NULL,
  accounttype varchar(32) NOT NULL,
  currency varchar(12) NOT NULL,
  blocked bool NULL,
  closed bool NULL,
  createdate timestamptz NOT NULL,
  closedate timestamptz NULL,
  "comment" varchar(256) NULL,
  userinfoid int8 NOT NULL,
  "version" int4 NULL,
  restriction varchar(16) NULL,
  CONSTRAINT account_pkey_s PRIMARY KEY (accountid),
  CONSTRAINT account_userinfoid_fkey_s FOREIGN KEY (userinfoid) REFERENCES public.userinfo(userinfoid) ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (
  OIDS=FALSE
) ;
CREATE INDEX fki_userinfo_account_s ON public.account (userinfoid int8_ops) ;

пример записи:
INSERT INTO public.account
(accountid, account, "name", accounttype, currency, blocked, closed, createdate, closedate, "comment", userinfoid, "version", restriction)
VALUES(5464, 'U0762515', 'USD', 'INTERNAL', 'USD', false, false, '2014-10-31 15:56:09.744', NULL, 'Automatically created account', 762, 0, NULL);

Записей в таблице - 300кк.

CREATE TABLE public.accountbalance (
  accountbalanceid int8 NOT NULL,
  balancefact numeric(24,8) NOT NULL DEFAULT 0,
  balanceplan numeric(24,8) NOT NULL DEFAULT 0,
  balancedate timestamptz NULL,
  "version" int4 NULL,
  CONSTRAINT accountbalance_pkey_s PRIMARY KEY (accountbalanceid)
)
WITH (
  OIDS=FALSE
) ;

Пример записи: INSERT INTO public.accountbalance
(accountbalanceid, balancefact, balanceplan, balancedate, "version")
VALUES(5464, 10000.00000000, 10000.00000000, '2014-10-31 15:56:09.744', 0);


Список счетов клиента живет в account. У клиента может быть несколько счетов с разными валютами (currency): RUB,USD,EUR,... и типами (accounttype): INERNAL,TRADE,TAX,...). Ключ пользователя - userinfoid. Остальные поля в рамках задания не используются.

Балансы по счетам живут в accountbalance, accountbalanceid - ключ из account, balancedate - время последнего изменения, в рамках задания. Numeric(24,8) - сознательно, все балансы с точностью до 8 зников, в double\int64 не все балансы влезают. Связь account-accountbalance - один-к-одному.

Характер нагрузки - на запись большинство счетов используется реже раза в сутки, ~10% - раз-несколько в сутки, < 0.1% (много меньше) - практически непрерывный поток. На чтение счета используются в 3-50 раз чаще, чем на запись.

2.

Нужен сервис, выставляющий rest api:
2.1. GET: getbalance(userinfoid, currency, accounttype) - возвращает соответствующий balancefact (просто число) или null, если такого счета нет.
2.2. GET: getbalances(userinfoid) - возвращает баланс всех счетов клиента в виде списка (Currency:String, AccountType:String, Balance:BigDecimal), или пустой список, если счетов у клиента нет.
2.3. POST: transact(List(UserInfoIdFrom:Long, AccountTypeFrom:String, UserInfoIdTo:Long, AccountTypeFrom:String, Currency:String, Amount:BigDecimal, DocumentId: String)) - проводит одну\несколько транзакций между счетами, если в результате не образуется счетов с отрицательным балансом. На выходе - (Error:Boolean,ErroCode:Option[String],ErrorText:Option[String]). При проведении транзакции одинаковые суммы добавляются \ вычитаются из balancefact и balanceplan, ни одно из них не должно становиться отрицательным.
2.4. тестовый клиент, который будет дергать getbalance/transact (перевод user1:TRADE -> user2:INTERNAL, 1 USD + user2:TRADE -> user1:INTERNAL 60 RUB), с указанием диапазона id клиентов и примерного rps.


Основные требования:
1) атомарность обработки transact (либо выполняются все транзакции, либо никакие);
2) transact отдает управление только после коммита в БД.
3) getbalance/gebalances отдают только данные, которые уже закомитчены в БД. Если запросы transact/getbalance пришли "одновременно", порядок выполнения не важен.
4) отсутствие deadlock'ов.
5) максимальный rps (без "нечитабельности кода").

Дополнительно (понятно, что за два дня все это скорее не успеть, чем успеть. Все указано в порядке приоритета):
6) адекватное кеширование;
7) предложение по смене структуры\типа БД для этого сервиса;
8) в случае отсутствия счетов, указанных в transact - создавать их (когда на них идет пополнение);
9) гарантированная атомарность получения балансов в getbalances;
10) добавка более шустрого канала для transact в тестовый клиент и сервис (grpc, например)
11) метод rollbacktrans(DocumentId: String) для "отката" транзакции, необходимые изменения\добавления в БД;
12) метрики на методы.