import sbt.Keys.libraryDependencies

lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.9"

lazy val protocol = (project in file("protocol"))
  .settings(name := "protocol",
    organization := "test.task",
    version := "1.0",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion
//      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion
    )
)
lazy val server = (project in file("server")).
  settings(
    inThisBuild(List(
      organization    := "test.task",
      scalaVersion    := "2.12.4"
    )),
    name := "testtask",
    version := "1.0",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,
      "org.scalikejdbc" %% "scalikejdbc"       % "3.2.0",
      "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
      "ch.qos.logback"  %  "logback-classic"   % "1.2.3",

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.1"         % Test
    )
  )
.dependsOn(protocol)

lazy val client = (project in file("client"))
  .settings(name := "client",
    organization := "test.task",
    version := "1.0",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion
    )
  )
.dependsOn(protocol)
