package server

import akka.actor.{ ActorRef, ActorSystem, PoisonPill }
import akka.event.Logging
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, StatusCodes }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{ get, post }
import akka.pattern.ask
import akka.util.Timeout
import protocol.Action._
import protocol.Response.{ BalanceResult, BalanceResults, TransactResult }

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.control.NonFatal
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.MediaTypes._
import protocol.{ Domain, JsonSupport }
import spray.json._

//#user-routes-class
trait Routes extends JsonSupport {
  //#user-routes-class

  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem

  implicit def executionContext: ExecutionContext

  lazy val log = Logging(system, classOf[Routes])

  // other dependencies that UserRoutes use
  def accountsActor: ActorRef = system.actorOf(AccountsActor.props)

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  lazy val userRoutes: Route = pathSingleSlash {
    get {
      complete {
        HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to akka-http</h1>")
      }
    }
  } ~
    pathPrefix("getbalance") {
      get {
        parameters('userinfoid.as[Long], 'currency.as[String], 'accounttype.as[String]) {
          (userinfoid, currency, accounttype) ⇒
            {
              val accountsActorInstance = accountsActor
              val result = accountsActorInstance ? Balance(userinfoid, currency, accounttype)
              result andThen {
                case _ ⇒
                  accountsActorInstance ! PoisonPill
              }
              onSuccess(result.map {
                case Some(b: BalanceResult) ⇒ HttpResponse(
                  StatusCodes.OK,
                  entity = HttpEntity(`application/json`, b.toJson.toString())
                )
                case None ⇒ HttpResponse(
                  StatusCodes.NotFound,
                  entity = HttpEntity(`application/json`, "Счет не обнаружен")
                )
                case _ ⇒ HttpResponse(
                  StatusCodes.InternalServerError,
                  entity = HttpEntity(`application/json`, "Ошибка обработки результата")
                )
              } recover {
                case NonFatal(e) ⇒ HttpResponse(
                  StatusCodes.InternalServerError,
                  entity = HttpEntity(`application/json`, e.getMessage)
                )
              }) {
                response: HttpResponse =>
                  complete(response)
              }
            }
        }
      }
    } ~
    pathPrefix("getbalances") {
      get {
        parameters('userinfoid.as[Long]) {
          userinfoid ⇒
            {
              val accountsActorInstance = accountsActor
              val result = accountsActorInstance ? Balances(userInfoId = userinfoid)
              result andThen {
                case _ ⇒
                  accountsActorInstance ! PoisonPill
              }
              onSuccess(result.map {
                case b: BalanceResults ⇒ HttpResponse(
                  StatusCodes.OK,
                  entity = HttpEntity(`application/json`, b.toJson.toString())
                )
                case _ ⇒ HttpResponse(
                  StatusCodes.InternalServerError,
                  entity = HttpEntity(`application/json`, "Ошибка обработки результата")
                )
              } recover {
                case NonFatal(e) ⇒ HttpResponse(
                  StatusCodes.InternalServerError,
                  entity = HttpEntity(`application/json`, e.getMessage)
                )
              }) {
                response: HttpResponse =>
                  complete(response)
              }
            }
        }
      }
    } ~
    pathPrefix("transact") {
      post {
        entity(as[Transact]) { transact ⇒
          val accountsActorInstance = accountsActor
          val result = accountsActorInstance ? transact
          result andThen {
            case _ ⇒
              accountsActorInstance ! PoisonPill
          }
          onSuccess(result.map {
            case tr: TransactResult ⇒ HttpResponse(
              StatusCodes.OK,
              entity = HttpEntity(`application/json`, tr.toJson.toString())
            )
            case _ ⇒ HttpResponse(
              StatusCodes.InternalServerError,
              entity = HttpEntity(`application/json`, "Ошибка обработки результата")
            )
          } recover {
            case NonFatal(e) ⇒ HttpResponse(
              StatusCodes.InternalServerError,
              entity = HttpEntity(`application/json`, e.getMessage)
            )
          }) {
            response: HttpResponse =>
              complete(response)
          }
        }
      }
    }
}
