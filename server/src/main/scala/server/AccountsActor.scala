package server

import akka.actor.{ Actor, ActorLogging, Props }
import akka.pattern.pipe
import protocol.Action._
import protocol.Response.{ BalanceResult, BalanceResults, TransactResult }

import scala.concurrent.Future
import scala.util.{ Failure, Success, Try }

object AccountsActor {
  def props: Props = Props[AccountsActor]
}

class AccountsActor extends Actor with ActorLogging {

  import context.dispatcher
  import scalikejdbc._

  def receive: Receive = {
    case balance @ Balance(userInfoId: Long, currency: String, accountType: String) ⇒ {
      log.debug(s"balance for $userInfoId")
      Future {
        DB readOnly { implicit session: DBSession =>
          findBalance(balance)
        }
      } pipeTo sender()
    }
    case Balances(userInfoId: Long) ⇒ {
      Future {
        val results = DB readOnly { implicit session: DBSession =>
          findBalances(userInfoId)
        }
        BalanceResults(balances = results)
      } pipeTo sender()
    }
    case Transact(operations: Seq[TransactOperation]) ⇒ {
      Future {
        import scala.util.{ Failure, Success, Try }
        import scalikejdbc.TxBoundary.Try._
        import scalikejdbc._
        val result: Try[Int] = DB localTx { implicit session: DBSession =>
          Try {
            processOperations(operations).map(_.get).sum
          }
        }
        result match {
          case Success(n) ⇒
            log.debug(s"Транзакция завершена, затронуто строк: $n")
            TransactResult(error = false, errorCode = None, errorText = None)
          case Failure(e) ⇒
            log.error(s"Ошибка выполнения транзакции:${e.getMessage},$e")
            TransactResult(error = true, errorCode = None, errorText = Some(e.getMessage))
        }
      }
    } pipeTo sender()
  }

  private def findBalance(balance: Balance)(implicit session: DBSession): Option[BalanceResult] = {
    sql"""SELECT a.accounttype, a.currency, ab.balancefact
	            FROM public.account a
              JOIN public.accountbalance ab ON a.accountid= ab.accountbalanceid
              WHERE a.userinfoid = ${balance.userInfoId} AND a.currency = ${balance.currency} AND a.accounttype = ${balance.accountType}"""
      .map(rs => BalanceResult(currency = rs.string("currency"), accountType = rs.string("accounttype"), balance = rs.long("balancefact")))
      .headOption()
      .apply()
  }

  private def findBalances(userInfoId: Long)(implicit session: DBSession): List[BalanceResult] = {
    sql"""SELECT a.accounttype, a.currency, ab.balancefact
	        FROM public.account a
          JOIN public.accountbalance ab ON a.accountid= ab.accountbalanceid
          WHERE a.userinfoid = $userInfoId"""
      .map(rs => BalanceResult(currency = rs.string("currency"), accountType = rs.string("accounttype"), balance = rs.long("balancefact")))
      .list()
      .apply()
  }

  private def findBalanceFactAndBalancePlan(account: Account)(implicit session: DBSession): Option[(BigDecimal, BigDecimal)] = {
    sql"""SELECT ab.balancefact, ab.balanceplan
          FROM public.account a
          JOIN public.accountbalance ab  ON ab.accountbalanceid = a.accountid
          WHERE  a.userinfoid = ${account.userInfoId}
          AND a.accounttype = ${account.accountType}
          AND a.currency = ${account.currency}""".map(
      rs ⇒ (BigDecimal(rs.bigDecimal("balancefact")), BigDecimal(rs.bigDecimal("balanceplan")))
    ).headOption().apply()
  }

  private def processOperation(operation: TransactOperation)(implicit session: DBSession): Try[Int] = {
    if(operation.from==operation.to) {
      Success(0)
    } else {
      val fromRowsAffected: Int =
        updateBalance(operation.from, sqls" - ${operation.amount}")
      val toRowsAffected: Int =
        updateBalance(operation.to, sqls" + ${convertCurrency(operation)}")
      if (fromRowsAffected != 1 || toRowsAffected != 1) {
        Failure(new Exception(s"Ошибка транзакции: колицество строк затронутых при обновлении источника $fromRowsAffected и места назначение $toRowsAffected не равно 1"))
      } else {
        Success(fromRowsAffected + toRowsAffected)
      }
    }
  }

  private def updateBalance(account: Account, operation: SQLSyntax)(implicit session: DBSession): Int = {
    sql"""UPDATE accountbalance ab
          SET balancefact = BALANCEFACT $operation,
              balanceplan = BALANCEPLAN $operation
          FROM account a
          WHERE a.accountid = ab.accountbalanceid
            AND a.userinfoid = ${account.userInfoId}
            AND a.accounttype = ${account.accountType}
            AND a.currency = ${account.currency}
      """.update().apply()
  }

  private def convertCurrency(operation: TransactOperation): BigDecimal = {
    (operation.from.currency, operation.to.currency, operation.amount) match {
      case ("USD", "RUB", amount) ⇒ amount * 60
      case ("RUB", "USD", amount) ⇒ amount / 60
      case (currency1, currency2, amount) if currency1 == currency2 ⇒ amount
    }
  }

  private def processOperations(operations: Seq[TransactOperation])(implicit session: DBSession): Seq[Try[Int]] = {
    operations.map {
      operation ⇒
        findBalanceFactAndBalancePlan(operation.from) match {
          case Some((balancefact: BigDecimal, balanceplan: BigDecimal)) if checkBalance(balancefact, operation.amount) &&
            checkBalance(balanceplan, operation.amount) ⇒
            processOperation(operation)
          case Some(_) ⇒
            Failure[Int](new Exception(s"Недостаточно средств"))
          case _ ⇒
            Failure[Int](new Exception("Ошибка получения баланса"))
        }
    }
  }

  def something[A](xs: Seq[Try[A]]): Try[Seq[A]] =
    Try(xs.map(_.get))

  private def checkBalance(balance: BigDecimal, amount: BigDecimal): Boolean = {
    balance > BigDecimal(0) && amount <= balance
  }

  override def preStart() {
    log.debug("actor started")
  }

  override def postStop() {
    log.debug("actor stopped")
  }
}
