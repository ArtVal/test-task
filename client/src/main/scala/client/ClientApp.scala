package client

import java.util.Random

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl.Source
import akka.stream.{ ActorMaterializer, ThrottleMode }
import protocol.Action.{ Account, Transact, TransactOperation }
import protocol.JsonSupport
import protocol.Response.{ BalanceResult, BalanceResults, TransactResult }
import spray.json._

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }
import scala.io.StdIn
import scala.util.control.NonFatal

object ClientApp extends App with JsonSupport {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  def getBalance(account: Account): Future[BalanceResult] = {
    Http()
      .singleRequest(
        HttpRequest(
          method = HttpMethods.GET,
          uri = s"http://localhost:8080/getbalance?userinfoid=${account.userInfoId}&currency=${account.currency}&accounttype=${account.accountType}"
        )
      ).flatMap((response: HttpMessage) ⇒ Unmarshal(response.entity).to[BalanceResult])
  }

  def getBalances(userInfoId: Long): Future[BalanceResults] = {
    Http()
      .singleRequest(
        HttpRequest(
          method = HttpMethods.GET,
          uri = s"http://localhost:8080/getbalances?userinfoid=$userInfoId"
        )
      ).flatMap((response: HttpMessage) ⇒ Unmarshal(response.entity()).to[BalanceResults])
  }

  def transact(transact: Transact): Future[TransactResult] = {
    Http()
      .singleRequest(
        HttpRequest(
          method = HttpMethods.POST,
          uri = s"http://localhost:8080/transact",
          entity = HttpEntity(`application/json`, transact.toJson.toString())
        )
      ).flatMap((response: HttpMessage) ⇒ Unmarshal(response.entity()).to[TransactResult])
  }

  val actions: Seq[() ⇒ Future[String]] = Seq[() ⇒ Future[String]](
    () ⇒ {
      getBalance(Account(userInfoId = 762L, accountType = "INTERNAL", currency = "USD"))
        .map(_ ⇒ "Get balaqnce Success").recover {
          case NonFatal(e) ⇒ "Get balance Failure"
        }
    },
    () ⇒ {
      transact(Transact(operations = Seq(
        TransactOperation(
          from = Account(userInfoId = 762L, accountType = "TAX", currency = "USD"),
          to = Account(userInfoId = 763L, accountType = "INTERNAL", currency = "RUB"),
          1
        ),
        TransactOperation(
          from = Account(userInfoId = 763L, accountType = "INTERNAL", currency = "RUB"),
          to = Account(userInfoId = 762L, accountType = "TAX", currency = "USD"),
          60
        )
      ))).map((result: TransactResult) ⇒ if (result.error) {
        "Transact failure"
      } else {
        "Transact Success"
      }).recover {
        case NonFatal(e) ⇒ "Transact error"
      }
    }
  )

  val rand = new Random(System.currentTimeMillis())
  Source.tick(0.millis, 0.millis, ()).grouped(10).throttle(1000, 1.second, 1, ThrottleMode.shaping)
    .mapAsync(10) {
      _ ⇒
        actions(rand.nextInt(actions.length))()
    }.runForeach(println)

  println("Press RETURN to stop...")

  StdIn.readLine()

  system.terminate()

}
